<?php
session_start();
if(!isset($_SESSION['user_name'])){
       header('Location:Login.php');
	   }
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <title>SalesApp</title>

        <!-- Bootstrap CSS CDN -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <!-- Our Custom CSS -->
        <link rel="stylesheet" href="css/style.css">
        <!-- Scrollbar Custom CSS -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.min.css">
        
    </head>
    <body>

        <div class="wrapper">
            <!-- Sidebar Holder -->
               <nav id="sidebar">
                <div id="dismiss">
                    <i class="glyphicon glyphicon-arrow-left"></i>
                </div>

                <div class="sidebar-header">
                    <h3>Sales App</h3>
                </div>
                <ul class="list-unstyled components">
					<li>
                    <a href="index.php">Home</a>
                        
                    </li>
					<li>
                        <a href="storesDashboard.php">Stores Dashboard</a>
                        
                    </li>
                    <li>
                        <a href="productDashboard.php">Products Dashboard</a>
                    </li>
                    <li>
                        <a href="salesdashboard.php">Sales Representatives</a>
                       
                    </li>
                    <li>
                        <a href="workAllocationDashboard.php">Work Allocation</a>
                    </li>
                    <li>
                       <?php echo '<a href="dashboard.php?status=error">'.$_SESSION["user_name"].'</a>';?>
                    </li>
					<li>
                        <a href="signout.php">Sign Out</a>
                    </li>
                </ul>     
            </nav>

            <!-- Page Content Holder -->
            <div id="content">

                <nav class="navbar navbar-default">
                    <div class="container-fluid">

                        <div class="navbar-header">
                            <button type="button" id="sidebarCollapse" class="btn btn-info navbar-btn">
                                <i class="glyphicon glyphicon-align-left"></i>
                                <span>Open Sidebar</span>
                            </button>
                        </div>

                        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                            
                        </div>
                    </div>
                </nav>
				  <?php
            if(!isset($_GET['mode'])){?>
        <div class="row">
			<div class="panel panel-default">
					<div class="panel-heading">
						 <a class="panel-title collapsed" data-toggle="collapse" data-parent="#panel-602160" href="#sales1">Sales Representative</a>
					</div>
					<div id="sales1" class="panel-collapse collapse">
						<div class="panel-body" >
							<table class="table" id="sales"> <tr><td>No RFecord Found</td></tr></table>
						</div>
					</div>
				</div>
				</div>
			<div class="row">
		<div class="col-md-6 col-md-offset-3">
			<form class="form-horizontal" role="form" action="signin.php" method="post">
					 <div class="form-group">
					<label for="inputEmail3" class="col-sm-2 control-label">
						Representative Name
					</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" id="name" name="name1" required/>
					</div>
				</div>
				<div class="form-group">
					<label for="inputEmail4" class="col-sm-2 control-label">
						Email
					</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" id="email" name="email" required/>
					</div>
				</div>
				<div class="form-group">
					<label for="inputEmail5" class="col-sm-2 control-label">
						PhoneNumber
					</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" id="phone" name="phone" required/>
					</div>
				</div>
				<div class="form-group">
					 
					<label for="inputPassword3" class="col-sm-2 control-label">
						Password
					</label>
					<div class="col-sm-10">
						<input type="password" class="form-control" id="pswd" name="pswd" required/>
					</div>
				</div>
				
				<div class="form-group">
					<div class="col-sm-offset-2 col-sm-10">
						<button type="submit" class="btn btn-primary">
							Add
						</button>
					</div>
				</div>
			</form>
		</div>
	</div>
				<?php 
				}
                else if($_GET['mode']=='view'){
                    require("newdbconfig.php");
					            $k=$_GET['rid'];
                     $sql="select * from representatives where rid=".$k;
                     $result = mysqli_query($conn,$sql);
                     $products= mysqli_fetch_assoc($result);
               echo '<div class="row">
                        <div class="col-md-6 col-md-offset-3">
                        <form class="form-horizontal">
                        <div class="form-group">
                        <label for="sname" class="col-sm-2 control-label"><span class="glyphicon glyphicon-home"></span></label>
                        <div class="col-sm-10">
                    <input type="text" class="form-control" id="pname" value='.$products["name"].' readonly>
                        </div>
                    </div>
                    <div class="form-group">
                    <label for="name" class="col-sm-2 control-label"><span class="glyphicon glyphicon-user"></span></label>
                    <div class="col-sm-10">
                    <input type="text" class="form-control" id="ptype" value='.$products["email"].' readonly>
                        </div>
                        </div>
                <div class="form-group">
                    <label for="inputBranch" class="col-sm-2 control-label"><span class="glyphicon glyphicon-bitcoin"></span></label>
                <div class="col-sm-10">
                <input type="text" class="form-control" id="ptype" value='.$products["phone"].' readonly>
                </div>
                </div>
            </form></div></div>';
         }
        else if($_GET['mode']=='edit'){
            require("newdbconfig.php");
					 $k=$_GET['rid'];
                     $sql="select * from representatives where rid=".$k;
                     $result = mysqli_query($conn,$sql);
                     $products= mysqli_fetch_assoc($result);
               echo '<div class="row">
                        <div class="col-md-6 col-md-offset-3">
                        <form class="form-horizontal">
						<input type="hidden" class="form-control" id="urid" value='.$products["rid"].' >
                        <div class="form-group">
                        <label for="sname" class="col-sm-2 control-label"><span class="glyphicon glyphicon-home"></span></label>
                        <div class="col-sm-10">
                    <input type="text" class="form-control" id="uname" value='.$products["name"].' >
                        </div>
                    </div>
                    <div class="form-group">
                    <label for="name" class="col-sm-2 control-label"><span class="glyphicon glyphicon-user"></span></label>
                    <div class="col-sm-10">
                    <input type="text" class="form-control" id="uemail" value='.$products["email"].' >
                        </div>
                        </div>
                <div class="form-group">
                    <label for="inputBranch" class="col-sm-2 control-label"><span class="glyphicon glyphicon-bitcoin"></span></label>
                <div class="col-sm-10">
                <input type="text" class="form-control" id="uphone" value='.$products["phone"].'>
				</br></br>
			   </div>
                </div>
				
            </form><center><button class="btn btn-default" onclick="myFun3()" >Save</button></center></div></div>
			';
            
        }
					
					
		?>				
</div></div>
        <div class="overlay" ></div>


        <!-- jQuery CDN -->
        <script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
        <!-- Bootstrap Js CDN -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <!-- jQuery Custom Scroller CDN -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.concat.min.js"></script>

        <script type="text/javascript">
		 var sales=document.getElementById('sales');
            $(document).ready(function () {
                $("#sidebar").mCustomScrollbar({
                    theme: "minimal"
                });
				$('#show1').hide();
                $('#dismiss, .overlay').on('click', function () {
                    $('#sidebar').removeClass('active');
                    $('.overlay').fadeOut();
                });

                $('#sidebarCollapse').on('click', function () {
                    $('#sidebar').addClass('active');
                    $('.overlay').fadeIn();
                    $('.collapse.in').toggleClass('in');
                    $('a[aria-expanded=true]').attr('aria-expanded', 'false');
                });
				 $.ajax({
                  url:"verify.php",
                  dataType: 'Json',
                  success: function(data){
					  var k=JSON.parse(data[2]);
                      var temp="<thead><tr><td>STORE NAME</td><td>MANAGER NAME</td><TD>phone</td><td>ACTION</td></tr></thead><tbody>";
                      if(k[0]!=null){
					  sales.innerHTML="";
                      for(var i=0;i<k.length;i++){
                       temp+="<tr><td>"+k[i]["name"]+"</td><td>"+k[i]["email"]+"</td><td>"+k[i]["phone"]+"</td><td><input type='button' class=\"btn btn-primary\"   onclick=\"location='salesDashboard.php?mode=view&rid="+k[i]["rid"]+"'\" value='VIEW'></input> &nbsp;<input type='button' class=\"btn btn-danger\"   onclick=\"location='salesDashboard.php?mode=edit&rid="+k[i]["rid"]+"'\" value='EDIT'></input></td></tr>";
                      }
                      temp+="</tbody></table>";
                      sales.innerHTML=temp;
                      }
				  }
            });
			});
		function myFun3(){
                var urid=document.getElementById('urid').value;
				var uname=document.getElementById('uname').value;
                var uemail=document.getElementById('uemail').value;
				var uphone=document.getElementById('uphone').value;
                 $.ajax({
        url:"verify.php",
        dataType: 'text',
        data:{'urid':urid,'uname':uname,'uemail':uemail,'uphone':uphone},
        success: function(data){
			window.location.href="dashboard.php?status=rerror";
        }
        });
        }
        </script>
    </body>
</html>