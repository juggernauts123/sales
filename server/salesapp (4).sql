-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 31, 2018 at 05:51 PM
-- Server version: 10.1.29-MariaDB
-- PHP Version: 7.2.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `salesapp`
-- --------------------------------------------------------

--
-- Table structure for table `job_master`
--

CREATE TABLE `job_master` (
  `JID` int(11) NOT NULL,
  `Day` date NOT NULL,
  `UID` int(11) NOT NULL,
  `product` varchar(250) NOT NULL,
  `quantity` varchar(250) NOT NULL,
  `StoresList` varchar(250) NOT NULL,
  `Status5` varchar(50) NOT NULL,
  `RID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `product_master`
--

CREATE TABLE `product_master` (
  `ProductName` varchar(50) NOT NULL,
  `PID` int(11) NOT NULL,
  `ProductType` varchar(50) NOT NULL,
  `UID` int(11) DEFAULT NULL,
  `CreatedDate` date NOT NULL,
  `Status2` varchar(50) NOT NULL,
  `UpdatedDate` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `product_price`
--

CREATE TABLE `product_price` (
  `PID` int(11) NOT NULL,
  `Qty` int(11) NOT NULL,
  `weigh` varchar(50) NOT NULL,
  `Price` float NOT NULL,
  `Date` date NOT NULL,
  `Status3` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `representatives`
--

CREATE TABLE `representatives` (
  `usertype` varchar(50) NOT NULL,
  `rid` int(11) NOT NULL,
  `UID` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `phone` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `stores_master`
--

CREATE TABLE `stores_master` (
  `SID` int(11) NOT NULL,
  `ManagerName` varchar(50) NOT NULL,
  `Address` varchar(50) NOT NULL,
  `Branch` varchar(50) NOT NULL,
  `Store_Name` varchar(50) NOT NULL,
  `Phone` int(11) NOT NULL,
  `UID` int(11) DEFAULT NULL,
  `CreatedDate` date NOT NULL,
  `Status1` varchar(50) NOT NULL DEFAULT '',
  `UpdatedDate` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `storetype`
--

CREATE TABLE `storetype` (
  `PID` int(11) DEFAULT NULL,
  `ID` int(11) NOT NULL,
  `expiryperiod` int(11) NOT NULL,
  `unit` varchar(50) NOT NULL,
  `storetype` varchar(50) NOT NULL,
  `minquat` int(11) NOT NULL,
  `maxquat` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `store_limits`
--

CREATE TABLE `store_limits` (
  `SID` int(11) NOT NULL,
  `PID` int(11) DEFAULT NULL,
  `LimitQty` int(11) NOT NULL,
  `Status4` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user_profile_master`
--

CREATE TABLE `user_profile_master` (
  `UserType` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `phone` int(11) NOT NULL,
  `UserName` varchar(50) NOT NULL,
  `Password` varchar(50) NOT NULL,
  `UID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `job_master`
--
ALTER TABLE `job_master`
  ADD PRIMARY KEY (`JID`),
  ADD KEY `FK_job_master_user_profile_master` (`UID`),
  ADD KEY `FK_job_master_representatives` (`RID`);

--
-- Indexes for table `product_master`
--
ALTER TABLE `product_master`
  ADD PRIMARY KEY (`PID`),
  ADD UNIQUE KEY `ProductName_SID` (`ProductName`),
  ADD KEY `FK_product_master_user_profile_master` (`UID`);

--
-- Indexes for table `product_price`
--
ALTER TABLE `product_price`
  ADD PRIMARY KEY (`PID`,`Qty`,`Price`);

--
-- Indexes for table `representatives`
--
ALTER TABLE `representatives`
  ADD PRIMARY KEY (`rid`),
  ADD KEY `FK_representatives_user_profile_master` (`UID`);

--
-- Indexes for table `stores_master`
--
ALTER TABLE `stores_master`
  ADD PRIMARY KEY (`SID`),
  ADD UNIQUE KEY `Phone` (`Phone`,`ManagerName`,`Branch`,`Store_Name`),
  ADD KEY `FK_stores_master_user_profile_master` (`UID`);

--
-- Indexes for table `storetype`
--
ALTER TABLE `storetype`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `FK_storetype_product_master` (`PID`);

--
-- Indexes for table `store_limits`
--
ALTER TABLE `store_limits`
  ADD PRIMARY KEY (`SID`),
  ADD KEY `FK_store_limits_product_master` (`PID`);

--
-- Indexes for table `user_profile_master`
--
ALTER TABLE `user_profile_master`
  ADD PRIMARY KEY (`UID`),
  ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `job_master`
--
ALTER TABLE `job_master`
  MODIFY `JID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `product_master`
--
ALTER TABLE `product_master`
  MODIFY `PID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `representatives`
--
ALTER TABLE `representatives`
  MODIFY `rid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `stores_master`
--
ALTER TABLE `stores_master`
  MODIFY `SID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `storetype`
--
ALTER TABLE `storetype`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `user_profile_master`
--
ALTER TABLE `user_profile_master`
  MODIFY `UID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `job_master`
--
ALTER TABLE `job_master`
  ADD CONSTRAINT `FK_job_master_representatives` FOREIGN KEY (`RID`) REFERENCES `representatives` (`rid`),
  ADD CONSTRAINT `FK_job_master_user_profile_master` FOREIGN KEY (`UID`) REFERENCES `user_profile_master` (`UID`);

--
-- Constraints for table `product_master`
--
ALTER TABLE `product_master`
  ADD CONSTRAINT `FK_product_master_user_profile_master` FOREIGN KEY (`UID`) REFERENCES `user_profile_master` (`UID`);

--
-- Constraints for table `product_price`
--
ALTER TABLE `product_price`
  ADD CONSTRAINT `FK_product_price_product_master` FOREIGN KEY (`PID`) REFERENCES `product_master` (`PID`);

--
-- Constraints for table `representatives`
--
ALTER TABLE `representatives`
  ADD CONSTRAINT `FK_representatives_user_profile_master` FOREIGN KEY (`UID`) REFERENCES `user_profile_master` (`UID`);

--
-- Constraints for table `stores_master`
--
ALTER TABLE `stores_master`
  ADD CONSTRAINT `FK_stores_master_user_profile_master` FOREIGN KEY (`UID`) REFERENCES `user_profile_master` (`UID`);

--
-- Constraints for table `storetype`
--
ALTER TABLE `storetype`
  ADD CONSTRAINT `FK_storetype_product_master` FOREIGN KEY (`PID`) REFERENCES `product_master` (`PID`);

--
-- Constraints for table `store_limits`
--
ALTER TABLE `store_limits`
  ADD CONSTRAINT `FK_store_limits_product_master` FOREIGN KEY (`PID`) REFERENCES `product_master` (`PID`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
