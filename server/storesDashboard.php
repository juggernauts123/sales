<?php
session_start();
if(!isset($_SESSION['user_name'])){
       header('Location:Login.php');
	   }
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <title>SalesApp</title>

        <!-- Bootstrap CSS CDN -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <!-- Our Custom CSS -->
        <link rel="stylesheet" href="css/style.css">
        <!-- Scrollbar Custom CSS -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.min.css">
        
    </head>
    <body>

        <div class="wrapper">
            <!-- Sidebar Holder -->
               <nav id="sidebar">
                <div id="dismiss">
                    <i class="glyphicon glyphicon-arrow-left"></i>
                </div>

                <div class="sidebar-header">
                    <h3>Sales App</h3>
                </div>
                <ul class="list-unstyled components">
					<li>
                    <a href="index.php">Home</a>
                        
                    </li>
					<li>
                        <a href="storesDashboard.php">Stores Dashboard</a>
                        
                    </li>
                    <li>
                        <a href="productDashboard.php">Products Dashboard</a>
                    </li>
                    <li>
                        <a href="salesdashboard.php">Sales Representatives</a>
                       
                    </li>
                    <li>
                        <a href="workAllocationDashboard.php">Work Allocation</a>
                    </li>
                    <li>
                       <?php echo '<a href="dashboard.php?status=error">'.$_SESSION["user_name"].'</a>';?>
                    </li>
					<li>
                        <a href="signout.php">Sign Out</a>
                    </li>
                </ul>     
            </nav>

            <!-- Page Content Holder -->
            <div id="content">

                <nav class="navbar navbar-default">
                    <div class="container-fluid">

                        <div class="navbar-header">
                            <button type="button" id="sidebarCollapse" class="btn btn-info navbar-btn">
                                <i class="glyphicon glyphicon-align-left"></i>
                                <span>Open Sidebar</span>
                            </button>
                        </div>

                        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                            
                        </div>
                    </div>
                </nav>
				  <?php
            if(!isset($_GET['mode'])){?>
			<div class="row">
			<div class="panel panel-default">
					<div class="panel-heading">
						 <a class="panel-title collapsed" data-toggle="collapse" data-parent="#panel-602160" href="#store1">Store Details</a>
					</div>
					<div id="store1" class="panel-collapse collapse">
						<div class="panel-body" >
                            <table class="table" id="store"> <tr><td>No RFecord Found</td></tr></table>
						</div>
					</div>
				</div>
			</div>
                <div class="row" id="show">
                <center><button class="btn btn-primary">ADD STORE</button></center>
                </div>
                <div class="row" id="show1"><br/><br/>
                <div class="col-md-6 col-md-offset-3">
            <form class="form-horizontal">
                 <div class="form-group">
                <label for="sname" class="col-sm-2 control-label"><span class="glyphicon glyphicon-home"></span></label>
                <div class="col-sm-10">
                <input type="text" class="form-control" id="sname" placeholder="Store Name">
                </div>
                </div>
                <div class="form-group">
                <label for="name" class="col-sm-2 control-label"><span class="glyphicon glyphicon-user"></span></label>
                <div class="col-sm-10">
                <input type="text" class="form-control" id="name" placeholder="Manager Name">
                </div>
                </div>
                <div class="form-group">
                <label for="Addr" class="col-sm-2 control-label"><span class="glyphicon glyphicon-envelope"></span></label>
                <div class="col-sm-10">
                <input type="text" class="form-control" id="Addr" placeholder="Address">
                </div>
                </div>
                <div class="form-group">
                    <label for="inputBranch" class="col-sm-2 control-label"><span class="glyphicon glyphicon-bitcoin"></span></label>
                <div class="col-sm-10">
                <input type="text" class="form-control" id="inputBranch" placeholder="Branch">
                </div>
                </div>
                 <div class="form-group">
                    <label for="inputPhone" class="col-sm-2 control-label"><span class="glyphicon glyphicon-phone-alt"></span></label>
                <div class="col-sm-10">
                <input type="number" class="form-control" id="inputPhone" placeholder="Mobile Number">
                </div>
                </div>
				<div class="form-group">
				 <div class="col-sm-offset-2 col-sm-10" id="div_quotes3">
                <select  id="sel1">
				<?php 
				require("newdbconfig.php");
				$name=$_SESSION['user_name'];
				$query="select p.ProductName from product_master p,user_profile_master u where u.UID=p.UID and u.email='$name';";
				$result = mysqli_query($conn,$query);
				echo '<option selected disabled>Select Products</option>';
				while($row = mysqli_fetch_array($result)){
					echo "<option>".$row['ProductName']."</option>";
				}
				?>
				</select>
				<input type="text" id="wt" placeholder="Enter Product Quantity">
				<input type="button" value="Add" onclick="addpr()"><br/><br/>
                </div>
                </div>
            </form>
                    <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                <button class="btn btn-default" onclick="myFun()">Submit</button>
                </div>
            </div>
                </div>
                
                
                </div> 
				<?php 
				}
                else if($_GET['mode']=='view'){
                    require("newdbconfig.php");
					            $k=$_GET['sid'];
                     $sql="select store_name,managername,address,branch,phone,status1,updateddate from stores_master where SID=".$k;
					 $sql1="select p.productname,s.limitqty from product_master p,store_limits s where p.PID=s.PID and s.SID=".$k;
                     $result = mysqli_query($conn,$sql);
					 $result1 = mysqli_query($conn,$sql1);
                     $products= mysqli_fetch_assoc($result);
               echo '<div class="row">
                        <div class="col-md-6 col-md-offset-3">
                        <form class="form-horizontal">
                        <div class="form-group">
                        <label for="sname" class="col-sm-2 control-label"><span class="glyphicon glyphicon-home"></span></label>
                        <div class="col-sm-10">
                    <input type="text" class="form-control" id="pname" value='.$products["store_name"].' readonly>
                        </div>
                    </div>
                    <div class="form-group">
                    <label for="name" class="col-sm-2 control-label"><span class="glyphicon glyphicon-user"></span></label>
                    <div class="col-sm-10">
                    <input type="text" class="form-control" id="ptype" value='.$products["managername"].' readonly>
                        </div>
                        </div>
                <div class="form-group">
                <label for="Addr" class="col-sm-2 control-label"><span class="glyphicon glyphicon-envelope"></span></label>
                <div class="col-sm-10">
                <input type="text" class="form-control" id="ptype" value='.$products["address"].' readonly>
                </div>
                </div>
                <div class="form-group">
                    <label for="inputBranch" class="col-sm-2 control-label"><span class="glyphicon glyphicon-bitcoin"></span></label>
                <div class="col-sm-10">
                <input type="text" class="form-control" id="ptype" value='.$products["branch"].' readonly>
                </div>
                </div>
				<div class="form-group">
                    <label for="inputBranch" class="col-sm-2 control-label"><span class="glyphicon glyphicon-bitcoin"></span></label>
                <div class="col-sm-10">
                <input type="text" class="form-control" id="ptype" value='.$products["phone"].' readonly>
                </div>
                </div>
				<div class="form-group">
                    <label for="inputBranch" class="col-sm-2 control-label"><span class="glyphicon glyphicon-bitcoin"></span></label>
                <div class="col-sm-10">
                <input type="text" class="form-control" id="ptype" value='.$products["status1"].' readonly>
                </div>
                </div>
				<div class="form-group">
                    <label for="inputBranch" class="col-sm-2 control-label"><span class="glyphicon glyphicon-bitcoin"></span></label>
                <div class="col-sm-10">
                <input type="text" class="form-control" id="ptype" value='.$products["updateddate"].' readonly>
                </div>
                </div>
            </form>
			  <table class="table table-hover">
    <thead>
      <tr>
        <th>Product Name</th>
        <th>Quantity Limited</th>
      </tr>
    </thead>
    <tbody>';
	while($row= mysqli_fetch_assoc($result1)){
		echo '<tr><td>'.$row['productname'].'</td>
		<td>'.$row['limitqty'].'</td></tr>';
	}
    echo '</tbody>
  </table>
            </div>
        </div>';
         }
        else if($_GET['mode']=='edit'){
			require("newdbconfig.php");
                     $k=$_GET['sid'];
                     $sql="select store_name,managername,address,branch,phone,status1,updateddate from stores_master where SID=".$k;
					 $sql1="select p.productname,s.limitqty from product_master p,store_limits s where p.PID=s.PID and s.SID=".$k;
                     $result = mysqli_query($conn,$sql);
					 $result1 = mysqli_query($conn,$sql1);
                     $products= mysqli_fetch_assoc($result);
      echo '<div class="row">
                        <div class="col-md-6 col-md-offset-3">
                        <form class="form-horizontal">
						<div class="form-group">
                        <div class="col-sm-10">
                    <input type="hidden" class="form-control" id="ssid" value='.$k.' >
                        </div>
                    </div>
                        <div class="form-group">
                        <label for="sname" class="col-sm-2 control-label"><span class="glyphicon glyphicon-home"></span></label>
                        <div class="col-sm-10">
                    <input type="text" class="form-control" id="ssname" value='.$products["store_name"].' >
                        </div>
                    </div>
                    <div class="form-group">
                    <label for="name" class="col-sm-2 control-label"><span class="glyphicon glyphicon-user"></span></label>
                    <div class="col-sm-10">
                    <input type="text" class="form-control" id="smname" value='.$products["managername"].' >
                        </div>
                        </div>
                <div class="form-group">
                <label for="Addr" class="col-sm-2 control-label"><span class="glyphicon glyphicon-envelope"></span></label>
                <div class="col-sm-10">
                <input type="text" class="form-control" id="saddr" value='.$products["address"].' >
                </div>
                </div>
                <div class="form-group">
                    <label for="inputBranch" class="col-sm-2 control-label"><span class="glyphicon glyphicon-bitcoin"></span></label>
                <div class="col-sm-10">
                <input type="text" class="form-control" id="sbranch" value='.$products["branch"].' >
                </div>
                </div>
				<div class="form-group">
                    <label for="inputBranch" class="col-sm-2 control-label"><span class="glyphicon glyphicon-bitcoin"></span></label>
                <div class="col-sm-10">
                <input type="text" class="form-control" id="sphone" value='.$products["phone"].' >
                </div>
                </div>
				<div class="form-group">
                    <label for="inputBranch" class="col-sm-2 control-label"><span class="glyphicon glyphicon-bitcoin"></span></label>
                <div class="col-sm-10">
                <input type="text" class="form-control" id="sstatus" value='.$products["status1"].' >
                </div>
                </div>
				<div class="form-group">
                    <label for="inputBranch" class="col-sm-2 control-label"><span class="glyphicon glyphicon-bitcoin"></span></label>
                <div class="col-sm-10">
                <input type="text" class="form-control" id="supdate" value='.$products["updateddate"].'>
                </div>
                </div>
            </form>
			  <table class="table table-hover">
    <thead>
    <tr>
        <th>Product Name</th>
        <th>Quantity Limited</th>
      </tr>
    </thead>
    <tbody>';
	while($row= mysqli_fetch_assoc($result1)){
		echo '<tr><td><input  type="text" class="form-control spuname" value="'.$row['productname'].'" readonly></td>
		<td><input  type="text" class="form-control spuqty" value="'.$row['limitqty'].'"></td></tr>';
	}
    echo '</tbody>
  </table>
  <button class="btn btn-default" onclick="myFun2()" >Save</button>
            </div>
        </div>';
            
        }
					
					
		?>				
</div></div>
        <div class="overlay"></div>


        <!-- jQuery CDN -->
        <script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
        <!-- Bootstrap Js CDN -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <!-- jQuery Custom Scroller CDN -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.concat.min.js"></script>

        <script type="text/javascript">
		var stores=document.getElementById('store');
            $(document).ready(function () {
                $("#sidebar").mCustomScrollbar({
                    theme: "minimal"
                });
				$('#show1').hide();
                $('#dismiss, .overlay').on('click', function () {
                    $('#sidebar').removeClass('active');
                    $('.overlay').fadeOut();
                });

                $('#sidebarCollapse').on('click', function () {
                    $('#sidebar').addClass('active');
                    $('.overlay').fadeIn();
                    $('.collapse.in').toggleClass('in');
                    $('a[aria-expanded=true]').attr('aria-expanded', 'false');
                });
				$("#show").click(function(){
    $("#show1").show();
	
		});
		$.ajax({
                  url:"verify.php",
                  dataType: 'Json',
                  success: function(data){
					  var k=JSON.parse(data[1]);
                      var temp="<thead><tr><td>STORE NAME</td><td>MANAGER NAME</td><TD>bRANCH</td><td>ACTION</td></tr></thead><tbody>";
                      if(k[0]!=null){
                      stores.innerHTML="";
                      for(var i=0;i<k.length;i++){
                       temp+="<tr><td>"+k[i]["Store_Name"]+"</td><td>"+k[i]["ManagerName"]+"</td><td>"+k[i]["Branch"]+"</td><td><input type='button' class=\"btn btn-primary\"   onclick=\"location='storesDashboard.php?mode=view&sid="+k[i]["SID"]+"'\" value='VIEW'></input> &nbsp;<input type='button' class=\"btn btn-danger\"   onclick=\"location='storesDashboard.php?mode=edit&sid="+k[i]["SID"]+"'\" value='EDIT'></input></td></tr>";
                      }
                      temp+="</tbody></table>";
                      stores.innerHTML=temp;
                      }
				  }
	});
            });
            function myFun(){
				  var qn3=$(".qn").map(function() {
                    return $(this).val();
                    }).get();
				var sel4=$(".sel").map(function() {
                    return $(this).val();
                    }).get();
				 var sname=document.getElementById('sname').value;
                var name1=document.getElementById('name').value;
                var Address=document.getElementById('Addr').value;
                var branch=document.getElementById('inputBranch').value;
                var phone=document.getElementById('inputPhone').value;
                 $.ajax({
        url:"verify.php",
        dataType: 'text',
        data:{'name1':name1,'sname':sname,'address':Address,'branch':branch,'phone':phone},
        success: function(data){
		
		var sid=parseInt(data);
        $.ajax({
        url:"verify.php",
        dataType: 'text',
        data:{'sid':sid,'qn3':qn3,'sel4':sel4},
        success: function(data){
			window.location.href="dashboard.php?status=ssuccess";
        }
        });
        }
    });
            }
			function myFun2(){
                var ssname=document.getElementById('ssname').value;
				var ssid=document.getElementById('ssid').value;
                var smname=document.getElementById('smname').value;
				var saddr=document.getElementById('saddr').value;
				var supdate=document.getElementById('supdate').value;
				var sphone=document.getElementById('sphone').value;
				var sstatus=document.getElementById('sstatus').value;
				var sbranch=document.getElementById('sbranch').value;
				var spuname=$(".spuname").map(function() {
                    return $(this).val();
                    }).get();
				var spuqty=$(".spuqty").map(function() {
                return $(this).val();
                    }).get();
                 $.ajax({
        url:"verify.php",
        dataType: 'text',
        data:{'ssname':ssname,'smname':smname,'sbranch':sbranch,'saddr':saddr,'supdate':supdate,'sphone':sphone,'sstatus':sstatus,'spuname':spuname,'spuqty':spuqty,'ssid':ssid},
        success: function(data){
			window.location.href="dashboard.php?status=serror";
        }
        });
        }
					function addpr(){
			var k=document.getElementById("wt").value;
			document.getElementById("wt").value = " ";
			var k2=document.getElementById("sel1").value;
			document.getElementById("sel1").value = " ";
            var div = document.getElementById('div_quotes3');
            div.innerHTML += "<input type=\"number\" min=0 name='wt[]'  class='qn'  value="+k+" ></input>";
			div.innerHTML += "<input  type=\"text\" minm=0 name='sel[]' class='sel' value="+k2+" ></input>";
            div.innerHTML += "<br/>";
}
        </script>
    </body>
</html>