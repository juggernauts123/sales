<?php
session_start();
if(!isset($_SESSION['user_name'])){
       header('Location:Login.php');
	   }
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <title>Collapsible sidebar using Bootstrap 3</title>

        <!-- Bootstrap CSS CDN -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <!-- Our Custom CSS -->
        <link rel="stylesheet" href="css/style.css">
        <!-- Scrollbar Custom CSS -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.min.css">
        
    </head>
    <body>

        <div class="wrapper">
            <!-- Sidebar Holder -->
         <nav id="sidebar">
                <div id="dismiss">
                    <i class="glyphicon glyphicon-arrow-left"></i>
                </div>

                <div class="sidebar-header">
                    <h3>Sales App</h3>
                </div>
                <ul class="list-unstyled components">
					<li>
                    <a href="index.php">Home</a>
                        
                    </li>
					<li>
                        <a href="storesDashboard.php">Stores Dashboard</a>
                        
                    </li>
                    <li>
                        <a href="productDashboard.php">Products Dashboard</a>
                    </li>
                    <li>
                        <a href="salesdashboard.php">Sales Representatives</a>
                       
                    </li>
                    <li>
                        <a href="workAllocationDashboard.php">Work Allocation</a>
                    </li>
                    <li>
                       <?php echo '<a href="dashboard.php?status=error">'.$_SESSION["user_name"].'</a>';?>
                    </li>
					<li>
                        <a href="signout.php">Sign Out</a>
                    </li>
                </ul>     
            </nav>
            <!-- Page Content Holder -->
            <div id="content">

                <nav class="navbar navbar-default">
                    <div class="container-fluid">

                        <div class="navbar-header">
                            <button type="button" id="sidebarCollapse" class="btn btn-info navbar-btn">
                                <i class="glyphicon glyphicon-align-left"></i>
                                <span>Open Sidebar</span>
                            </button>
                        </div>

                        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                            
                        </div>
                    </div>
                </nav>
                <?php
            if(!isset($_GET['mode'])){?>
			<div class="row">
				<div class="panel panel-default">
					<div class="panel-heading">
						 <a class="panel-title collapsed" data-toggle="collapse" data-parent="#panel-602160" href="#pro1">Products Details</a>
					</div>
					<div id="pro1" class="panel-collapse collapse">
						<div class="panel-body" >
                            <table class="table" id="pro"> <tr><td>No RFecord Found</td></tr></table>
						</div>
					</div>
                    </div></div>
                <div class="row" id="show">
                <center><button class="btn btn-primary">ADD Product</button></center>
                </div>
                <div class="row" id="show1"><br/><br/>
                <div class="col-md-6 col-md-offset-3">
            <form class="form-horizontal">
                 <div class="form-group">
                <label for="sname" class="col-sm-2 control-label"><span class="glyphicon glyphicon-home"></span></label>
                <div class="col-sm-10">
                <input type="text" class="form-control" id="pname" placeholder="Product Name">
                </div>
                </div>
                <div class="form-group">
                <label for="name" class="col-sm-2 control-label"><span class="glyphicon glyphicon-user"></span></label>
                <div class="col-sm-10">
                <input type="text" class="form-control" id="ptype" placeholder="Product Type">
                </div>
                </div>
            </form>
                  <div class="form-group">
           
			 <div id="text">
			 <span class="label label-default">Avaliable Quantities</span>
                <div class="col-sm-offset-2 col-sm-10" id="div_quotes3">
                <input type="text" id="wt" placeholder="Enter Product Quantity">
				<input type="text" id="wt1" placeholder="Enter Product Price">
				<select  id="sel1">
				<option selected disabled>Select Weight Type</option>
				<option>gm</option>
				<option>kg</option>
				</select>
	<input type="button" value="Add" onclick="addpr()"><br/><br/>
                </div>
				 <div class="col-sm-offset-2 col-sm-10">
                <button class="btn btn-default" onclick="myFun()" id="driver">Next</button>
                </div>
            </div>
			 </div>
                </div>
                
                
                </div>   
				<div class="row" id="show3">
                <div class="col-md-6 col-md-offset-3">
			 <div  id="text"><br/><br/>
                <div class="col-sm-offset-2 col-sm-10">
                <input type="text" id="exp" placeholder="Enter Expiry Period">
				<select id="mon">
				<option selected disabled>Select Type</option>
				<option>Days</option>
				<option>Months</option>
				<option>Years</option>
				</select>
                </div><br/><br/>
				<div class="col-sm-offset-2 col-sm-10" id="div_quotes4">
				<select  id="sel2">
				<option selected disabled>Select Store Type</option>
				<option>Large</option>
				<option>Medium</option>
				<option>Small</option>
				</select>
                <input type="text" id="wt2" placeholder="Enter Minimum Quantity">
				<input type="text" id="wt3" placeholder="Enter Maximum Price">
				<input type="button" value="Add" onclick="addpr1()"><br/><br/>
                </div>
				 <div class="col-sm-offset-2 col-sm-10">
                <button class="btn btn-default" onclick="myFun1()" id="driver">Submit</button>
                </div>
            </div>
			 </div>
                
                
                </div> 
                <?php 
				}
                else if($_GET['mode']=='view'){
                    require("newdbconfig.php");
                    $k=$_GET['pid'];
                     $sql="select ProductName, ProductType,status2,updateddate from product_master where PID=".$k;
					 $sql1="select qty,weigh,Price from product_price where PID=".$k;
					 $sql2="select * from storetype where PID=".$k;
                     $result = mysqli_query($conn,$sql);
					 $result1 = mysqli_query($conn,$sql1);
					 $result2 = mysqli_query($conn,$sql2);
                     $products= mysqli_fetch_assoc($result);
               echo '<div class="row">
                        <div class="col-md-6 col-md-offset-3">
                        <form class="form-horizontal">
                        <div class="form-group">
                        <label for="sname" class="col-sm-2 control-label"><span class="glyphicon glyphicon-home"></span></label>
                        <div class="col-sm-10">
                    <input type="text" class="form-control" id="pname" value='.$products["ProductName"].' readonly>
                        </div>
                    </div>
                    <div class="form-group">
                    <label for="name" class="col-sm-2 control-label"><span class="glyphicon glyphicon-user"></span></label>
                    <div class="col-sm-10">
                    <input type="text" class="form-control" id="ptype" value='.$products["ProductType"].' readonly>
                        </div>
                        </div>
                <div class="form-group">
                <label for="Addr" class="col-sm-2 control-label"><span class="glyphicon glyphicon-envelope"></span></label>
                <div class="col-sm-10">
                <input type="text" class="form-control" id="ptype" value='.$products["status2"].' readonly>
                </div>
                </div>
                <div class="form-group">
                    <label for="inputBranch" class="col-sm-2 control-label"><span class="glyphicon glyphicon-bitcoin"></span></label>
                <div class="col-sm-10">
                <input type="text" class="form-control" id="ptype" value='.$products["updateddate"].' readonly>
                </div>
                </div>
            </form>
			  <table class="table table-hover">
    <thead>
      <tr>
        <th>Quantity</th>
        <th>Price</th>
      </tr>
    </thead>
    <tbody>';
	while($row= mysqli_fetch_assoc($result1)){
		echo '<tr><td>'.$row['qty'].$row['weigh'].'</td>
		<td>'.$row['Price'].'Rs'.'</td></tr>';
	}
    echo '</tbody>
  </table>
  	  <table class="table table-hover">
    <thead>
      <tr>
        <th>Expiry period(From Manufactured)</th>
		<th>StoreType</th>
		<th>Minimum Quantity</th>
		<th>Maximum Quantity</th>
      </tr>
    </thead>
    <tbody>';
	while($row2= mysqli_fetch_assoc($result2)){
		echo '<tr><td>'.$row2['expiryperiod'].$row2['unit'].'</td>
		<td>'.$row2['storetype'].'</td>
		<td>'.$row2['minquat'].'</td>
		<td>'.$row2['maxquat'].'</td></tr>';
	}
	echo '</tbody>
  </table>
            </div>
        </div>';
         }
        else if($_GET['mode']=='edit'){
			require("newdbconfig.php");
                   $k=$_GET['pid'];
                     $sql="select ProductName, ProductType,status2,updateddate from product_master where PID=".$k;
					 $sql1="select qty,weigh,Price from product_price where PID=".$k;
					 $sql2="select * from storetype where PID=".$k;
                     $result = mysqli_query($conn,$sql);
					 $result1 = mysqli_query($conn,$sql1);
					 $result2 = mysqli_query($conn,$sql2);
                     $products= mysqli_fetch_assoc($result);
               echo '<div class="row">
                        <div class="col-md-6 col-md-offset-3">
                        <form class="form-horizontal">
						<div class="form-group">
                        <div class="col-sm-10">
                    <input type="hidden" class="form-control" id="puid" value='.$k.' >
                        </div>
                    </div>
                        <div class="form-group">
                        <label for="sname" class="col-sm-2 control-label"><span class="glyphicon glyphicon-home"></span></label>
                        <div class="col-sm-10">
                    <input type="text" class="form-control" id="pname" value='.$products["ProductName"].' >
                        </div>
                    </div>
                    <div class="form-group">
                    <label for="name" class="col-sm-2 control-label"><span class="glyphicon glyphicon-user"></span></label>
                    <div class="col-sm-10">
                    <input type="text" class="form-control" id="ptype" value='.$products["ProductType"].' >
                        </div>
                        </div>
                <div class="form-group">
                <label for="Addr" class="col-sm-2 control-label"><span class="glyphicon glyphicon-envelope"></span></label>
                <div class="col-sm-10">
                <input type="text" class="form-control" id="pstatus" value='.$products["status2"].' >
                </div>
                </div>
                <div class="form-group">
                    <label for="inputBranch" class="col-sm-2 control-label"><span class="glyphicon glyphicon-bitcoin"></span></label>
                <div class="col-sm-10">
                <input type="text" class="form-control" id="pupdated" value='.$products["updateddate"].'>
                </div>
                </div>
            </form>
			  <table class="table table-hover">
    <thead>
      <tr>
        <th>Quantity</th>
        <th>Price(Rs)</th>
      </tr>
    </thead>
    <tbody>';
	while($row= mysqli_fetch_assoc($result1)){
		echo '<tr><td ><input type="text" class="form-control puqty" value="'.$row['qty'].$row['weigh'].'" readonly></td>
		<td ><input  type="text" class="form-control puprice" value="'.$row['Price'].'"></td></tr>';
	}
    echo '</tbody>
  </table>		
		 </table>
  	  <table class="table table-hover">
    <thead>
      <tr>
        <th>Expiry period(From Manufactured)</th>
		<th>StoreType</th>
		<th>Minimum Quantity</th>
		<th>Maximum Quantity</th>
      </tr>
    </thead>
    <tbody>';
	while($row2= mysqli_fetch_assoc($result2)){
		echo '<input type="hidden" class="ids" value='.$row2['ID'].'><tr>
		<td><input type="text" class="form-control exp1" value="'.$row2['expiryperiod'].$row2['unit'].'"</td>
		<td><input type="text" class="form-control store" value="'.$row2['storetype'].'"></td>
		<td><input type="text" class="form-control minquat" value="'.$row2['minquat'].'"></td>
		<td><input type="text" class="form-control maxquat" value="'.$row2['maxquat'].'"></td></tr>';
	}
	echo '</tbody>
  </table>
    <button class="btn btn-default" onclick="myFun2()" >Save</button>
            </div>
        </div>';
            
        }?></div></div>
        <div class="overlay"></div>


        <!-- jQuery CDN -->
        <script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
        <!-- Bootstrap Js CDN -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <!-- jQuery Custom Scroller CDN -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.concat.min.js"></script>

        <script type="text/javascript">
            $(document).ready(function () {
                $("#sidebar").mCustomScrollbar({
                    theme: "minimal"
                });

                $('#dismiss, .overlay').on('click', function () {
                    $('#sidebar').removeClass('active');
                    $('.overlay').fadeOut();
                });

                $('#sidebarCollapse').on('click', function () {
                    $('#sidebar').addClass('active');
                    $('.overlay').fadeIn();
                    $('.collapse.in').toggleClass('in');
                    $('a[aria-expanded=true]').attr('aria-expanded', 'false');
                });
							$('#show1').hide();
							$('#show3').hide();
				$("#show").click(function(){
				$("#show1").show();
				$('#show3').hide();
});
				$("#driver").click(function(){
				$("#show3").show();
				$("#show1").hide();
});
				var pro=document.getElementById('pro');
                var sales=document.getElementById('sales');
                var stores=document.getElementById('store');
                var work=document.getElementById('work');
             $.ajax({
                  url:"verify.php",
                  dataType: 'Json',
                  success: function(data){
                      var k=JSON.parse(data[0]);
                      var temp="<thead><tr><td>PRODUCT NAME</td><td>PRODUCT TYPE</td><td>ACTION</td></tr></thead><tbody>";
                      if(k[0]!=null){
                    
                      pro.innerHTML="";
                      for(var i=0;i<k.length;i++){
                       temp+="<tr><td>"+k[i]["ProductName"]+"</td><td>"+k[i]["ProductType"]+"</td><td><input type='button' class=\"btn btn-primary\" id=\"view_button"+k[i]["PID"]+"\"  onclick=\"location='productDashboard.php?mode=view&pid="+k[i]["PID"]+"'\" value='VIEW'></input> &nbsp;<input type='button' class=\"btn btn-danger\" id=\"edit_button"+k[i]["PID"]+"\"  onclick=\"location='productDashboard.php?mode=edit&pid="+k[i]["PID"]+"'\" value='EDIT'></input></td></tr>";
                      }
                      temp+="</tbody></table>";
                      pro.innerHTML=temp;
                      }
				  }
					   }); 
  });
          
            function myFun(){
                var pname=document.getElementById('pname').value;
                var ptype=document.getElementById('ptype').value;
                var qn=$(".qn").map(function() {
                    return $(this).val();
                    }).get();
				var prs=$(".prs").map(function() {
                    return $(this).val();
                    }).get();
				var sel=$(".sel").map(function() {
                    return $(this).val();
                    }).get();	
                 $.ajax({
        url:"verify.php",
        dataType: 'text',
        data:{'pname':pname,'ptype':ptype},
        success: function(data){
			console.log(data);
		var pid=parseInt(data);
        $.ajax({
        url:"verify.php",
        dataType: 'text',
        data:{'pid':pid,'prs':prs,'qn':qn,'sel':sel},
        success: function(data){
			window.location.href="dashboard.php?status=psuccess";
        }
        });
        }
                 });
			}
            
			  function myFun1(){
                var exp=document.getElementById('exp').value;
                var mon=document.getElementById('mon').value;
                var qn1=$(".qn1").map(function() {
                    return $(this).val();
                    }).get();
				var prs1=$(".prs1").map(function() {
                    return $(this).val();
                    }).get();
				var sel2=$(".sel2").map(function() {
                    return $(this).val();
                    }).get();	
                 $.ajax({
        url:"verify.php",
        dataType: 'text',
        data:{'exp':exp,'mon':mon,'prs1':prs1,'qn1':qn1,'sel2':sel2},
        success: function(data){
			console.log(data);
			window.location.href="dashboard.php?status=perror";
        }
        });
        }
			  function myFun2(){
                var puname=document.getElementById('pname').value;
                var putype=document.getElementById('ptype').value;
				var pustatus=document.getElementById('pstatus').value;
				var puupdated=document.getElementById('pupdated').value;
				var puid=document.getElementById('puid').value;
				var puqty=$(".puqty").map(function() {
                    return $(this).val();
                    }).get();
				var puprice=$(".puprice").map(function() {
                return $(this).val();
                    }).get();
				var minquat=$(".minquat").map(function() {
                return $(this).val();
                    }).get();
				var maxquat=$(".maxquat").map(function() {
                return $(this).val();
                    }).get();
				var store=$(".store").map(function() {
                return $(this).val();
                    }).get();
				var exp1=$(".exp1").map(function() {
                return $(this).val();
                    }).get();
				var ids=$(".ids").map(function() {
                return $(this).val();
                    }).get();
                 $.ajax({
        url:"verify.php",
        dataType: 'text',
        data:{'puname':puname,'putype':putype,'ids':ids,'pustatus':pustatus,'puupdated':puupdated,'puid':puid,'puqty':puqty,'puprice':puprice,'store':store,'maxquat':maxquat,'minquat':minquat,'exp1':exp1},
        success: function(data){
		window.location.href="dashboard.php?status=perror";
        }
        });
        }
			function addpr(){
			var k=document.getElementById("wt").value;
			document.getElementById("wt").value = " ";
			var k1=document.getElementById("wt1").value;
			document.getElementById("wt1").value = " ";
			var k2=document.getElementById("sel1").value;
			document.getElementById("sel1").value = " ";
            var div = document.getElementById('div_quotes3');
            div.innerHTML += "<input type=\"number\" min=0 name='wt[]'  class='qn'  value="+k+" ></input>";
			div.innerHTML += "<input  type=\"number\" minm=0 name='wt1[]' class='prs' value="+k1+" ></input>";
			div.innerHTML += "<input  type=\"text\" minm=0 name='sel[]' class='sel' value="+k2+" ></input>";
            div.innerHTML += "<br/>";
}
	function addpr1(){
			var k=document.getElementById("wt2").value;
			document.getElementById("wt2").value = " ";
			var k1=document.getElementById("wt3").value;
			document.getElementById("wt3").value = " ";
			var k2=document.getElementById("sel2").value;
			document.getElementById("sel2").value = " ";
            var div = document.getElementById('div_quotes4');
            div.innerHTML += "<input type=\"number\" min=0 name='wt2[]'  class='qn1'  value="+k+" ></input>";
			div.innerHTML += "<input  type=\"number\" minm=0 name='wt3[]' class='prs1' value="+k1+" ></input>";
			div.innerHTML += "<input  type=\"text\" minm=0 name='sel2[]' class='sel2' value="+k2+" ></input>";
            div.innerHTML += "<br/>";
}
            
        </script>
    </body>
</html>