<?php
session_start();
if(!isset($_SESSION['user_name'])){
       header('Location:Login.php');
	   }
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <title>Collapsible sidebar using Bootstrap 3</title>
        <!-- Bootstrap CSS CDN -->
        <script
  src="http://code.jquery.com/jquery-3.3.1.min.js"
  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
  crossorigin="anonymous"></script>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <!-- Our Custom CSS -->
        <link rel="stylesheet" href="css/style.css">
        <!-- Scrollbar Custom CSS -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.min.css">
        <script type="text/javascript" src="js/scripts.js"></script>
    </head>
    <body>

        <div class="wrapper">
            <!-- Sidebar Holder -->
            <nav id="sidebar">
                <div id="dismiss">
                    <i class="glyphicon glyphicon-arrow-left"></i>
                </div>

                <div class="sidebar-header">
                    <h3>Sales App</h3>
                </div>
                <ul class="list-unstyled components">
                   <li>
                    <a href="index.php">Home</a>
                        
                    </li>
					<li>
                        <a href="storesDashboard.php">Stores Dashboard</a>
                        
                    </li>
                    <li>
                        <a href="productDashboard.php">Products Dashboard</a>
                    </li>
                    <li>
                        <a href="salesdashboard.php">Sales Representatives</a>
                       
                    </li>
                    <li>
                        <a href="workAllocationDashboard.php">Work Allocation</a>
                    </li>
                    <li>
                       <?php echo '<a href="dashboard.php?status=error">'.$_SESSION["user_name"].'</a>';?>
                    </li>
					<li>
                        <a href="signout.php">Sign Out</a>
                    </li>
                </ul>

	     
            </nav>
        </div>
            <!-- Page Content Holder -->
            <div id="content">

                <nav class="navbar navbar-default">
                    <div class="container-fluid">

                        <div class="navbar-header">
                            <button type="button" id="sidebarCollapse" class="btn btn-info navbar-btn">
                                <i class="glyphicon glyphicon-align-left"></i>
                                <span>Open Sidebar</span>
                            </button>
                        </div>

                        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                            
                        </div>
                    </div>
                </nav>
			<?php
            if(!isset($_GET['mode'])){?>
				<div class="row">
					<div class="panel panel-default">
					<div class="panel-heading">
						 <a class="panel-title collapsed" data-toggle="collapse" data-parent="#panel-602160" href="#Allocation">Work Allocation</a>
					</div>
					<div id="Allocation" class="panel-collapse collapse">
						<div class="panel-body">
                        <table class="table" id="work"> <tr><td>No RFecord Found</td></tr></table>
						</div>
					</div>
				</div>
				</div>
                <div class="row" id="show">
                <center><button class="btn btn-primary">ADD WorkAllocation To SR</button></center>
                </div>
                <div class="row" id="show1"><br/><br/>
                <div class="col-md-6 col-md-offset-2">
            <form class="form-horizontal" method="post" action="#">
                 <div class="form-group">
                    <label for="sname" class="col-sm-2 control-label"><span class="glyphicon glyphicon-calendar"></span></label>
                    <div class="col-sm-10">
                        <input type="date" class="form-control" id="today" placeholder="Day" min="<?php echo date('Y-m-d');?>" required>
                     </div>
                </div>
                <div class="form-group">
                <label for="name" class="col-sm-2 control-label"><span class="glyphicon glyphicon-user"></span></label>
                <div class="col-sm-10">
                <select  class="form-control" id="representative">
                    <option selected disabled>Select Representatives</option>
                    <?php 
					require("newdbconfig.php");
					$email=$_SESSION['user_name'];
					$sql1="select r.rid, r.name from representatives r,user_profile_master u  where r.UID=u.UID and u.email='$email';";
					$result1 = mysqli_query($conn,$sql1);
					while($row1= mysqli_fetch_array($result1)){
						echo' <option value='.$row1['rid'].'>'.$row1['name'].'</option>';
					}
					?>
                    </select>
                </div>
                </div>
                <div class="form-group options">
                <label for="Addr" class="col-md-2 control-label col-sm-2"><span class="glyphicon glyphicon-road"></span></label>
                <div class="col-md-10 col-sm-10">
                    <div class="col-md-6 col-md-offset-2">
					<?php 
					require("newdbconfig.php");
					$email=$_SESSION['user_name'];
					$sql="select s.sid,s.Store_Name from stores_master s,user_profile_master u where u.UID=s.UID and u.email='$email';";
					$result = mysqli_query($conn,$sql);
					while($row = mysqli_fetch_array($result)){
						echo' <label class="checkbox-inline" for="Checkboxes_Apple">
                      <input type="checkbox" name="Checkboxes" id='.$row['sid'].' value='.$row['Store_Name'].' onclick="numbi(this)">
                      '.$row['Store_Name'].'
                    </label>';
					}
					?>
                    </div></div>
                </div>
                 <div  id="text" class="form-group">
                <div class="col-sm-offset-2 col-sm-10" id="div_quotes3" >
                <select  id="rep1" >
				<?php 
				require("newdbconfig.php");
				$name=$_SESSION['user_name'];
				$query="select p.ProductName from product_master p,user_profile_master u where u.UID=p.UID and u.email='$email';";
				$result = mysqli_query($conn,$query);
				echo '<option selected disabled>Select Products</option>';
				while($row = mysqli_fetch_array($result)){
					echo "<option>".$row['ProductName']."</option>";
				}
				?>
                </select>
				<input type="text" id="sel1" placeholder="Enter Product quantity">
	            <input type="button" value="Add" onclick="addpr()"><br/><br/>
                </div></div></form>
				 <div class="col-sm-offset-2 col-sm-10">
                <center>
                <button class="btn btn-default" onclick="myFun()" id="driver">Submit</button>
                </div></center>
                
            </div> 
                <div class="col-md-3"><ul class="list-group item-list" id="set">
                </ul>
                </div>
            </div>
               <?php 
				}
                else if($_GET['mode']=='view'){
           require("newdbconfig.php");
					$uname=$_SESSION['user_name'];
					$query="select UID from user_profile_master where email='$uname'";
					$res = mysqli_query($conn,$query);
					$r=mysqli_fetch_array($res);
					$uid=$r['UID'];
					 $k=$_GET['jid'];
                     $sql="select * from job_master where JID=".$k;
					 $sql1="select r.name from representatives r,job_master j where r.UID=j.UID and j.JID=".$k;
                     $result = mysqli_query($conn,$sql);
                     $products= mysqli_fetch_assoc($result);
					 $result1 = mysqli_query($conn,$sql1);
                     $products1= mysqli_fetch_assoc($result1);
					 $pro=($products["product"]);
               echo '<div class="row">
                        <div class="col-md-6 col-md-offset-3">
                        <form class="form-horizontal">
                        <div class="form-group">
                        <label for="sname" class="col-sm-2 control-label"><span class="glyphicon glyphicon-home"></span></label>
                        <div class="col-sm-10">
                    <input type="text" class="form-control" id="pname" value='.$products["Day"].' readonly>
                        </div>
                    </div>
                    <div class="form-group">
                    <label for="name" class="col-sm-2 control-label"><span class="glyphicon glyphicon-user"></span></label>
                    <div class="col-sm-10">
                    <input type="text" class="form-control" id="ptype" value='.trim($pro,"\"").' readonly>
                        </div>
                        </div>
                <div class="form-group">
                <label for="Addr" class="col-sm-2 control-label"><span class="glyphicon glyphicon-envelope"></span></label>
                <div class="col-sm-10">
                <input type="text" class="form-control" id="ptype" value='.trim($products["quantity"],"\"").' readonly>
                </div>
                </div>
                <div class="form-group">
                    <label for="inputBranch" class="col-sm-2 control-label"><span class="glyphicon glyphicon-bitcoin"></span></label>
                <div class="col-sm-10">
                <input type="text" class="form-control" id="ptype" value='.trim($products["StoresList"],"\"").' readonly>
                </div>
                </div>
				<div class="form-group">
                    <label for="inputBranch" class="col-sm-2 control-label"><span class="glyphicon glyphicon-bitcoin"></span></label>
                <div class="col-sm-10">
                <input type="text" class="form-control" id="ptype" value='.$products["Status5"].' readonly>
                </div>
                </div>
				<div class="form-group">
                    <label for="inputBranch" class="col-sm-2 control-label"><span class="glyphicon glyphicon-bitcoin"></span></label>
                <div class="col-sm-10">
                <input type="text" class="form-control" id="ptype" value='.$products1["name"].' readonly>
                </div>
                </div>
            </form>';
         }
        else if($_GET['mode']=='edit'){
			require("newdbconfig.php");
           	$uname=$_SESSION['user_name'];
					$query="select UID from user_profile_master where email='$uname'";
					$res = mysqli_query($conn,$query);
					$r=mysqli_fetch_array($res);
					$uid=$r['UID'];
					 $k=$_GET['jid'];
                     $sql="select * from job_master where JID=".$k;
					 $sql1="select r.name from representatives r,job_master j where r.UID=j.UID and j.JID=".$k;
                     $result = mysqli_query($conn,$sql);
                     $products= mysqli_fetch_assoc($result);
					 $result1 = mysqli_query($conn,$sql1);
                     $products1= mysqli_fetch_assoc($result1);
               echo '<div class="row">
                        <div class="col-md-6 col-md-offset-3">
                        <form class="form-horizontal">
						<input type="hidden" id="jid" class="form-control" id="jday" value='.$k.' >
                        <div class="form-group">
                        <label for="sname" class="col-sm-2 control-label"><span class="glyphicon glyphicon-home"></span></label>
                        <div class="col-sm-10">
                    <input type="text" class="form-control" id="jday" value='.$products["Day"].' >
                        </div>
                    </div>
                    <div class="form-group">
                    <label for="name" class="col-sm-2 control-label"><span class="glyphicon glyphicon-user"></span></label>
                    <div class="col-sm-10">
                    <input type="text" class="form-control" id="jproduct" value='.$products["product"].' >
                        </div>
                        </div>
                <div class="form-group">
                <label for="Addr" class="col-sm-2 control-label"><span class="glyphicon glyphicon-envelope"></span></label>
                <div class="col-sm-10">
                <input type="text" class="form-control" id="jquantity" value='.$products["quantity"].' >
                </div>
                </div>
                <div class="form-group">
                    <label for="inputBranch" class="col-sm-2 control-label"><span class="glyphicon glyphicon-bitcoin"></span></label>
                <div class="col-sm-10">
                <input type="text" class="form-control" id="jstore" value='.$products["StoresList"].' >
                </div>
                </div>
				<div class="form-group">
                    <label for="inputBranch" class="col-sm-2 control-label"><span class="glyphicon glyphicon-bitcoin"></span></label>
                <div class="col-sm-10">
                <input type="text" class="form-control" id="jstatus" value='.$products["Status5"].' >
                </div>
                </div>
				<div class="form-group">
                    <label for="inputBranch" class="col-sm-2 control-label"><span class="glyphicon glyphicon-bitcoin"></span></label>
                <div class="col-sm-10">
                <input type="text" class="form-control" id="jname" value='.$products1["name"].' readonly>
                </div>
                </div>
            </form>  <button class="btn btn-default" onclick="myFun4()" >Save</button>';
            
        }
					
					
		?>					
            </div>

        <div class="overlay"></div>


        <!-- jQuery CDN -->
        <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js"></script>
        <!-- Bootstrap Js CDN -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <!-- jQuery Custom Scroller CDN -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.concat.min.js"></script>

        <script type="text/javascript">
            var x=[];
			var work=document.getElementById('work');
            $(document).ready(function () {
                $("#sidebar").mCustomScrollbar({
                    theme: "minimal"
                });

                $('#dismiss, .overlay').on('click', function () {
                    $('#sidebar').removeClass('active');
                    $('.overlay').fadeOut();
                });
                $('#sidebarCollapse').on('click', function () {
                    $('#sidebar').addClass('active');
                    $('.overlay').fadeIn();
                    $('.collapse.in').toggleClass('in');
                    $('a[aria-expanded=true]').attr('aria-expanded', 'false');
                });
                $('.item-list').sortable();
				 $.ajax({
                  url:"verify.php",
                  dataType: 'Json',
				 success: function(data){
                      var k=JSON.parse(data[3]);
                      temp="<thead><tr><td>Rep NAME</td><td>Day</td><TD>phone</td><td>StoreList</td></tr></thead><tbody>";
                      if(k[0]!=null){
                      work.innerHTML="";
                      for(var i=0;i<k.length;i++){
                       temp+="<tr><td>"+k[i]["name"]+"</td><td>"+k[i]["Day"]+"</td><td>"+k[i]["StoresList"]+"</td><td><input type='button' class=\"btn btn-primary\"   onclick=\"location='workAllocationDashboard.php?mode=view&jid="+k[i]["JID"]+"'\" value='VIEW'></input> &nbsp;<input type='button' class=\"btn btn-danger\"   onclick=\"location='workAllocationDashboard.php?mode=edit&jid="+k[i]["JID"]+"'\" value='EDIT'></input></td></tr>";
                      }
                      temp+="</tbody></table>";
                      work.innerHTML=temp;
                      }
				 }
			});
                    
            });
            function myFun(){
                var d=document.getElementById('today').value;
                var rep=document.getElementById('representative').value;
                console.log(rep);
                var sortedIDs = $( ".item-list" ).sortable( "toArray" );
                var pr=$(".product").map(function() {
                    return $(this).val();
                    }).get();
				var qn=$(".qn").map(function() {
                    return $(this).val();
                    }).get();
                console.log(sortedIDs);
                 $.ajax({
                    url:"verify.php",
                    dataType: 'text',
                    data:{'dt':d,'rp':rep,'route':sortedIDs,'prod':pr,'qna':qn},
                    success: function(data){
                       window.location.href="dashboard.php?status=wsuccess";
                        }
            });
            }
			function myFun4(){
				var jid=document.getElementById('jid').value;
                var jday=document.getElementById('jday').value;
                var jproduct=document.getElementById('jproduct').value;
				var jquantity=document.getElementById('jquantity').value;
				var jstore=document.getElementById('jstore').value;
				var jstatus=document.getElementById('jstatus').value;
				//var rep=document.getElementById('jname').value;
                 $.ajax({
                    url:"verify.php",
                    dataType: 'text',
                    data:{'jday':jday,'jproduct':jproduct,'jquantity':jquantity,'jstore':jstore,'jstatus':jstatus,'jid':jid},
                    success: function(data){
                        window.location.href="dashboard.php?status=werror";
                        }
            });
            }
            var lg=document.getElementById('set');
            function numbi(ob){
                if(x.indexOf(ob.value)<0){
                    x.push(ob.value);
                    set.innerHTML+="<li class='list-group-item' id="+ob.value+">"+ob.value+"</li>";
                }
                else{
                    var y=document.getElementById(ob.value);
                    y.parentNode.removeChild(y);
                    x.splice(x.indexOf(ob.value),1);
                }
            }
          function addpr(){
			var k=document.getElementById("rep1").value;
			document.getElementById("rep1").value = " ";
			var k2=document.getElementById("sel1").value;
			document.getElementById("sel1").value = " ";
            var div = document.getElementById('div_quotes3');
            div.innerHTML += "<input type=\"text\" min=0 name='wt[]'  class='product'  value="+k+" ></input>";
			div.innerHTML += "<input  type=\"number\" minm=0 name='sel[]' class='qn' value="+k2+" ></input>";
            div.innerHTML += "<br/>";
}
        </script>
    </body>
</html>