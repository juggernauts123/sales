<?php
session_start();
if(!isset($_SESSION['user_name'])){
       header('Location:Login.php');
	   }
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <title>Collapsible sidebar using Bootstrap 3</title>

        <!-- Bootstrap CSS CDN -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <!-- Our Custom CSS -->
        <link rel="stylesheet" href="css/style.css">
        <!-- Scrollbar Custom CSS -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.min.css">
        
    </head>
    <body>

        <div class="wrapper">
            <!-- Sidebar Holder -->
            <nav id="sidebar">
                <div id="dismiss">
                    <i class="glyphicon glyphicon-arrow-left"></i>
                </div>

                <div class="sidebar-header">
                    <h3>Sales App</h3>
                </div>

                <ul class="list-unstyled components">
                    <p></p>
                    <li class="active">
                        <a href="index.php">Home</a>
                        
                    </li>
                    <li class="active">
                        <a href="storesDashboard.php">Stores Dashboard</a>
                        
                    </li>
                    <li>
                        <a href="productDashboard.php">Products Dashboard</a>
                    </li>
                    <li>
                        <a href="salesdashboard.php">Sales Representatives</a>
                       
                    </li>
                    <li>
                        <a href="workAllocationDashboard.php">Work Allocation</a>
                    </li>
                   <li>
                       <?php echo '<a href="dashboard.php?status=error">'.$_SESSION["user_name"].'</a>';?>
                    </li>
					<li>
                        <a href="signout.php">Sign Out</a>
                    </li>
                </ul>

              
            </nav>

            <!-- Page Content Holder -->
            <div id="content">

                <nav class="navbar navbar-default">
                    <div class="container-fluid">

                        <div class="navbar-header">
                            <button type="button" id="sidebarCollapse" class="btn btn-info navbar-btn">
                                <i class="glyphicon glyphicon-align-left"></i>
                                <span>Open Sidebar</span>
                            </button>
                        </div>

                        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                            
                        </div>
                    </div>
                </nav>
                <div class="row">
				<?php 
	if($_GET['status']=='success'){?>
				<div class="alert alert-success alert-dismissable">
  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
  <strong>Success!</strong> Logged In Successfully.
	</div><?php }?>
		<?php 
	if($_GET['status']=='rsuccess'){?>
				<div class="alert alert-success alert-dismissable">
  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
  <strong>Success!</strong> representatives added successfully.
	</div><?php }?>
		<?php 
	if($_GET['status']=='wsuccess'){?>
				<div class="alert alert-success alert-dismissable">
  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
  <strong>Success!</strong> work allocation added Successfully.
	</div><?php }?>
		<?php 
	if($_GET['status']=='ssuccess'){?>
				<div class="alert alert-success alert-dismissable">
  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
  <strong>Success!</strong> stores added Successfully.
	</div><?php }?>
		<?php 
	if($_GET['status']=='psuccess'){?>
				<div class="alert alert-success alert-dismissable">
  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
  <strong>Success!</strong> products added Successfully.
	</div><?php }?>
	<?php 
	if($_GET['status']=='rerror'){?>
				<div class="alert alert-success alert-dismissable">
  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
  <strong>Success!</strong> representatives data updated Successfully.
	</div><?php }?>
	<?php 
	if($_GET['status']=='perror'){?>
				<div class="alert alert-success alert-dismissable">
  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
  <strong>Success!</strong> Product data updated successfully.
	</div><?php }?>
	<?php 
	if($_GET['status']=='werror'){?>
				<div class="alert alert-success alert-dismissable">
  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
  <strong>Success!</strong> Work allocation data updated Successfully.
	</div><?php }?>
	<?php 
	if($_GET['status']=='serror'){?>
				<div class="alert alert-success alert-dismissable">
  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
  <strong>Success!</strong>stores data updated Successfully.
	</div><?php }?>
		<div class="col-md-12">
			<div class="panel-group" id="panel-602160">
				<div class="panel panel-default">
					<div class="panel-heading">
						 <a class="panel-title collapsed" data-toggle="collapse" data-parent="#panel-602160" href="#pro1">Products Details</a>
					</div>
					<div id="pro1" class="panel-collapse collapse">
						<div class="panel-body" >
                            <table class="table" id="pro"> <tr><td>No RFecord Found</td></tr></table>
						</div>
					</div>
                    </div>
				<div class="panel panel-default">
					<div class="panel-heading">
						 <a class="panel-title collapsed" data-toggle="collapse" data-parent="#panel-602160" href="#store1">Store Details</a>
					</div>
					<div id="store1" class="panel-collapse collapse">
						<div class="panel-body" >
                            <table class="table" id="store"> <tr><td>No RFecord Found</td></tr></table>
						</div>
					</div>
				</div>
                	<div class="panel panel-default">
					<div class="panel-heading">
						 <a class="panel-title collapsed" data-toggle="collapse" data-parent="#panel-602160" href="#sales1">Sales Representative</a>
					</div>
					<div id="sales1" class="panel-collapse collapse">
						<div class="panel-body" >
							<table class="table" id="sales"> <tr><td>No RFecord Found</td></tr></table>
						</div>
					</div>
				</div>
                	<div class="panel panel-default">
					<div class="panel-heading">
						 <a class="panel-title collapsed" data-toggle="collapse" data-parent="#panel-602160" href="#Allocation">Work Allocation</a>
					</div>
					<div id="Allocation" class="panel-collapse collapse">
						<div class="panel-body">
                        <table class="table" id="work"> <tr><td>No RFecord Found</td></tr></table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
                
            </div>
        </div>

        <div class="overlay"></div>


        <!-- jQuery CDN -->
        <script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
        <!-- Bootstrap Js CDN -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <!-- jQuery Custom Scroller CDN -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.concat.min.js"></script>

        <script type="text/javascript">
            $(document).ready(function () {
                $("#sidebar").mCustomScrollbar({
                    theme: "minimal"
                });

                $('#dismiss, .overlay').on('click', function () {
                    $('#sidebar').removeClass('active');
                    $('.overlay').fadeOut();
                });

                $('#sidebarCollapse').on('click', function () {
                    $('#sidebar').addClass('active');
                    $('.overlay').fadeIn();
                    $('.collapse.in').toggleClass('in');
                    $('a[aria-expanded=true]').attr('aria-expanded', 'false');
                });
                var pro=document.getElementById('pro');
                var sales=document.getElementById('sales');
                var stores=document.getElementById('store');
                var work=document.getElementById('work');
             $.ajax({
                  url:"verify.php",
                  dataType: 'Json',
                  success: function(data){
                      var k=JSON.parse(data[0]);
                      var temp="<thead><tr><td>PRODUCT NAME</td><td>PRODUCT TYPE</td><td>ACTION</td></tr></thead><tbody>";
                      if(k[0]!=null){
                    
                      pro.innerHTML="";
                      for(var i=0;i<k.length;i++){
                       temp+="<tr><td>"+k[i]["ProductName"]+"</td><td>"+k[i]["ProductType"]+"</td><td><input type='button' class=\"btn btn-primary\" id=\"view_button"+k[i]["PID"]+"\"  onclick=\"location='productDashboard.php?mode=view&pid="+k[i]["PID"]+"'\" value='VIEW'></input> &nbsp;<input type='button' class=\"btn btn-danger\" id=\"edit_button"+k[i]["PID"]+"\"  onclick=\"location='productDashboard.php?mode=edit&pid=1'\" value='EDIT'></input></td></tr>";
                      }
                      temp+="</tbody></table>";
                      pro.innerHTML=temp;
                      }
                      k=JSON.parse(data[1]);
                      temp="<thead><tr><td>STORE NAME</td><td>MANAGER NAME</td><TD>bRANCH</td><td>ACTION</td></tr></thead><tbody>";
                      if(k[0]!=null){
                      stores.innerHTML="";
                      for(var i=0;i<k.length;i++){
                       temp+="<tr><td>"+k[i]["Store_Name"]+"</td><td>"+k[i]["ManagerName"]+"</td><td>"+k[i]["Branch"]+"</td><td><input type='button' class=\"btn btn-primary\"   onclick=\"location='storesDashboard.php?mode=view&sid="+k[i]["SID"]+"'\" value='VIEW'></input> &nbsp;<input type='button' class=\"btn btn-danger\"   onclick=\"location='storesDashboard.php?mode=edit&sid="+k[i]["SID"]+"'\" value='EDIT'></input></td></tr>";
                      }
                      temp+="</tbody></table>";
                      stores.innerHTML=temp;
                      }
                    
                      k=JSON.parse(data[2]);
                      temp="<thead><tr><td>STORE NAME</td><td>MANAGER NAME</td><TD>phone</td><td>ACTION</td></tr></thead><tbody>";
                      if(k[0]!=null){
                      sales.innerHTML="";
                      for(var i=0;i<k.length;i++){
                       temp+="<tr><td>"+k[i]["name"]+"</td><td>"+k[i]["email"]+"</td><td>"+k[i]["phone"]+"</td><td><input type='button' class=\"btn btn-primary\"   onclick=\"location='salesDashboard.php?mode=view&rid="+k[i]["rid"]+"'\" value='VIEW'></input> &nbsp;<input type='button' class=\"btn btn-danger\"   onclick=\"location='salesDashboard.php?mode=edit&rid="+k[i]["rid"]+"'\" value='EDIT'></input></td></tr>";
                      }
                      temp+="</tbody></table>";
                      sales.innerHTML=temp;
                      }
                         k=JSON.parse(data[3]);
                      temp="<thead><tr><td>Rep NAME</td><td>Day</td><TD>phone</td><td>StoreList</td></tr></thead><tbody>";
                      if(k[0]!=null){
                      work.innerHTML="";
                      for(var i=0;i<k.length;i++){
                       temp+="<tr><td>"+k[i]["name"]+"</td><td>"+k[i]["Day"]+"</td><td>"+k[i]["StoresList"]+"</td><td><input type='button' class=\"btn btn-primary\"   onclick=\"location='workAllocationDashboard.php?mode=view&jid="+k[i]["JID"]+"'\" value='VIEW'></input> &nbsp;<input type='button' class=\"btn btn-danger\"   onclick=\"location='workAllocationDashboard.php?mode=edit&jid="+k[i]["JID"]+"'\" value='EDIT'></input></td></tr>";
                      }
                      temp+="</tbody></table>";
                      work.innerHTML=temp;
                      }
                  }
                      
            });
            });
        </script>
    </body>
</html>