<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Bootstrap 3, from LayoutIt!</title>

    <meta name="description" content="Source code generated using layoutit.com">
    <meta name="author" content="LayoutIt!">

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">

  </head>
   <body>
<div class="container-fluid">
    </br>
    </br>
    </br>
	<div class="row">
	<?php 
	if($_GET['status']=='error'){?>
	<div class="alert alert-success alert-dismissable">
  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
  <strong>Error!</strong> Please Sign Up to Continue.
	</div><?php }?>
		<div class="col-md-6 col-md-offset-3">
			<form class="form-horizontal" role="form" action="signin.php" method="post">
					 <div class="form-group">
					<label for="inputEmail3" class="col-sm-2 control-label">
						Name
					</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" id="name" name="name" required/>
					</div>
				</div>
				<div class="form-group">
					<label for="inputEmail4" class="col-sm-2 control-label">
						Email
					</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" id="email" name="email" required/>
					</div>
				</div>
				<div class="form-group">
					<label for="inputEmail5" class="col-sm-2 control-label">
						PhoneNumber
					</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" id="phone" name="phone" required/>
					</div>
				</div>
				<div class="form-group">
					 
					<label for="inputPassword3" class="col-sm-2 control-label">
						Password
					</label>
					<div class="col-sm-10">
						<input type="password" class="form-control" id="pswd" name="pswd" required/>
					</div>
				</div>
				
				<div class="form-group">
					<div class="col-sm-offset-2 col-sm-10">
						<button type="submit" class="btn btn-primary">
							Sign up
						</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
</body>
</html>