<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Bootstrap 3, from LayoutIt!</title>

    <meta name="description" content="Source code generated using layoutit.com">
    <meta name="author" content="LayoutIt!">

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
        <!-- Bootstrap CSS CDN -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <!-- Our Custom CSS -->
        <link rel="stylesheet" href="css/style.css">
        <!-- Scrollbar Custom CSS -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.min.css">

  </head>
   <body>
<div class="container-fluid">
    </br>
    </br>
    </br>
	<div class="row">
		<?php 
	if($_GET['status']=='success'){?>
				<div class="alert alert-success alert-dismissable">
  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
  <strong>Success!</strong> Signed In Successfully.
	</div><?php }?>
		<?php 
	if($_GET['status']=='error1'){?>
				<div class="alert alert-danger alert-dismissable">
  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
  <strong>Failed!</strong> Login details incorrect.
	</div><?php }?>
		<div class="col-md-6 col-md-offset-3">
					 <div class="form-group">
					<label for="inputEmail3" class="col-sm-2 control-label">
						User Name
					</label>
					<div class="col-sm-10">
						<input type="email" class="form-control" id="inputEmail3"  required/>
					</div>
				</div>
            </br>
				<div class="form-group">
					 
					<label for="inputPassword3" class="col-sm-2 control-label">
						Password
					</label>
					<div class="col-sm-10">
						<input type="password" class="form-control" id="inputPassword3" required/>
					</div>
				</div>
				
				<div class="form-group">
					<div class="col-sm-offset-2 col-sm-10">
						<button type="submit" class="btn btn-primary" onclick="myFun();">
							Sign In
						</button>
					</div>
				</div>
		</div>
	</div>
</div>
        <!-- jQuery CDN -->
        <script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
        <!-- Bootstrap Js CDN -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <!-- jQuery Custom Scroller CDN -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.concat.min.js"></script>
<script>
function myFun(){
    var name=document.getElementById('inputEmail3').value;
    var pwd=document.getElementById('inputPassword3').value;
    $.ajax({
        url:"verify.php",
        dataType: 'text',
        data:{'name':name,'value':pwd},
        success: function(data){
			console.log(data);
            if(data.trim()=='success'){
			window.location.href="dashboard.php?status=success";
            }
			else{
			window.location.href="Login.php?status=error1";
			}
        }
    });
}
</script>
</body>
</html>