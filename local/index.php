<?php
		session_start();		
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>SalesApp</title>

    <meta name="description" content="Source code generated using layoutit.com">
    <meta name="author" content="LayoutIt!">

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">

  </head>
 <nav class="navbar navbar-inverse navbar-fixed">
  <div class="container-fluid">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span> 
      </button>
      <a class="navbar-brand" href="#">SalesApp</a>
    </div>
       <div class="collapse navbar-collapse" id="myNavbar">
    <ul class="nav navbar-nav">
      <li><a href="index.php">Home</a></li>
      <li><a href="storesDashboard.php">Stores</a></li>
      <li><a href="productDashboard.php">Products</a></li>
	   <li><a href="workAllocationDashboard.php">WorkAllocation</a></li>
	   <li><a href="rep.html">Add Representatives</a></li>
    </ul>
	<?php

		if(isset($_SESSION['user_name'])){
		?>
    <ul class="nav navbar-nav navbar-right">
	 <?php echo'<li><a href="dashboard.php"><span class="glyphicon glyphicon-log-in"></span> '.$_SESSION["user_name"].'</a></li>';?>
      <li><a href="signout.php"><span class="glyphicon glyphicon-log-in"></span> Signout</a></li>
           </ul>
		   <?php } else{?>
		   <ul class="nav navbar-nav navbar-right">
      <li><a href="signUp.php?status=success"><span class="glyphicon glyphicon-user"></span>Sign Up</a></li>
      <li><a href="Login.php?status=error"><span class="glyphicon glyphicon-log-in"></span>Log In</a></li>
           </ul>
		   <?php }?>
		   </div>
  </div>
    </nav>
  <body id="myPage">

    <div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			<div class="jumbotron well">
				<h2>
					Hello, world!
				</h2>
				<p>
					This is a template for a simple marketing or informational website. It includes a large callout called the hero unit and three supporting pieces of content. Use it as a starting point to create something more unique.
				</p>
				<p>
					<a class="btn btn-primary btn-large" href="#">Learn more</a>
				</p>
			</div>
			<div class="carousel slide" id="carousel-449712">
				<ol class="carousel-indicators">
					<li class="active" data-slide-to="0" data-target="#carousel-449712">
					</li>
					<li data-slide-to="1" data-target="#carousel-449712">
					</li>
					<li data-slide-to="2" data-target="#carousel-449712">
					</li>
				</ol>
				<div class="carousel-inner">
					<div class="item active">
						<img alt="Carousel Bootstrap First" src="http://lorempixel.com/output/sports-q-c-1600-500-1.jpg">
						<div class="carousel-caption">
							<h4>
								First Thumbnail label
							</h4>
							<p>
								Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.
							</p>
						</div>
					</div>
					<div class="item">
						<img alt="Carousel Bootstrap Second" src="http://lorempixel.com/output/sports-q-c-1600-500-2.jpg">
						<div class="carousel-caption">
							<h4>
								Second Thumbnail label
							</h4>
							<p>
								Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.
							</p>
						</div>
					</div>
					<div class="item">
						<img alt="Carousel Bootstrap Third" src="http://lorempixel.com/output/sports-q-c-1600-500-3.jpg">
						<div class="carousel-caption">
							<h4>
								Third Thumbnail label
							</h4>
							<p>
								Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.
							</p>
						</div>
					</div>
				</div> <a class="left carousel-control" href="#carousel-449712" data-slide="prev"><span class="glyphicon glyphicon-chevron-left"></span></a> <a class="right carousel-control" href="#carousel-449712" data-slide="next"><span class="glyphicon glyphicon-chevron-right"></span></a>
			</div>
		</div>
	</div>
</div>

    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/scripts.js"></script>
<hr>
<footer class="container-fluid text-center">
<div class="container-fluid">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span> 
      </button>
      <a class="navbar-brand" href="#">SalesApp</a>
    </div>
       <div class="collapse navbar-collapse" id="myNavbar">
    <ul class="nav navbar-nav">
      <li class="active"><a href="#">Home</a></li>
      <li><a href="storesDashboard.php">Stores</a></li>
      <li><a href="productDashboard.php">Products</a></li>
	   <li><a href="workAllocationDashboard.php">WorkAllocation</a></li>
	   <li><a href="rep.html">Add Representatives</a></li>
    </ul>
	</div>
  </div>
  <a href="#myPage" title="To Top">
    <span class="glyphicon glyphicon-chevron-up"></span>
  </a>
  <p>SalesApp</p>
</footer>
  </body>
</html>